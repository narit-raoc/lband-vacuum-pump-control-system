# ระบบควบคุม Vacuum pump ในระบบ L-Band receiver (L band vacuum pump control system)

### เรื่อง คู่มือการใช้งานและรายละเอียดของโปรแกรมควาบคุม Vacuum pump ของ L-band receiver  

- จัดทำโดย  นายณัฐดนัย หารตันกูล  
- สังกัด     ศูนย์ปฏิบัติการดาศาสตร์วิทยุ  
- ตำแหน่ง   วิศวกร Firmware  
- วันที่จัดทำ  20220131
  
### วัตุประสงค์  
1. เพื่อให้ผู้อ่านสามารถเข้าใจการทำงานของระบบได้  
2. เพื่อให้ผู่อ่านสามารถใช้งานระบบได้อย่างถูกต้อง และเหมาะสม


### เอกสารแนบ
|ชื่อเอกสาร                                      |ความสำคัญ                                              |
|----------------------------------------------|------------------------------------------------------|
|UserManual.docx                               |แสดงสรุปภาพรวม และตัวระบบทั้งหมด                         |
|Note.txt                                      |บันทึกการ setup raspberry pi และ หน้าที่ของแต่ละ GPIO      |
|Leybold_Scrollvac_Plus_Operating_Manual.pdf   |Datasheet ของ Vacuum ที่ทำการควบคุม                     |
|**L_band_Pump.py                              |Python script ที่ทำการควบคุม และแสดงผล GUI              |
|**L_band_Pump_GUI.ui                          |ไฟล์หน้าต่าง GUI                                        |
|**L_Band__VacumnPumpControl.py                |Python script ที่ใช้ใน raspberry pi เพื่อควบคุม vacuum pump|

** โปรแกรมที่ใช้งาน



# User manual

### ส่วนต่างๆในหน้า GUI

![GUI_sector](./picture/GUI_sector.PNG)

รูปที่ 1 แสดงส่วนต่างๆ ของ GUI 


![state_0](./picture/state_0.PNG)

รูปที่ 2 แสดง initial GUI ที่ปกติ



### แผนภาพแสดงการเปลี่ยนแปลงตามปกติตามผลของการกดปุ่ม

![state_machine](./picture/state_machine.PNG)

รูปที่ 3 แสดงการเปลี่ยนแปลงตามปกติตามผลของการกดปุ่ม


### Pump status

![pump_status_table](./picture/pump_status_table.PNG)


เนื่องด้วยสถานะ ![FullSpeed](./picture/FullSpeed.PNG) ไม่สามารถตรวจสอบได้โดย software จึงแนะนะแนวทางการตรวจสอบว่า Pump อยู่ในสถานะ ไม่มีพลังงาน หรือกำลังทำงานเต็มที่(speed 100%)  โดยกดที่ปุ่ม speed 70% แล้วสังเกตว่า Pump status อยู่ในสถานะใด
1. สถานะ![pumpStatus_normal](./picture/pumpStatus_normal.PNG)  แสดงว่า Pump ปกติ หากกดปุ่ม speed 100% จะทำงานเต็มที่(speed 100%)  
2. สถานะ ![FullSpeed](./picture/FullSpeed.PNG)  แสดงว่า Pump ไม่มีพลังงาน หากกดปุ่ม speed 100% จะไม่ทำงาน


### Network Status
![network_status](./picture/network_status.PNG)


### Setup system
การติดตั้งโปรแกรม   
1.	ติดตั้ง script “L_Band__VacumnPumpControl.py” ใน raspberry pi และตั้งค่า ตามเอการแนบ “Note.txt”
2.	ติดตั้ง script “L_band_Pump.py” และ “L_band_Pump_GUI.ui” ใน PC
3.	ตั้ง IP address และ Port ของ raspberry pi เพื่อการสื่อสาร


### การใช้งาน
1.	เปิด raspberry pi (จะ run python script ที่ใช้ควบคุมโดยอัตโนมัต) และ GUI โดยลำดับการเปิดไม่มีผล
2.	สังเกตุการ GUI โดยสถานะเริ่มต้นของระบบจะถูกต้องค่าไว้ดังรูปที่ 2 (สถานะปิด ใน speed 70%)
3.	สังเกตุหาก Network Status แสดงสถานะ ![networStatus_connect](./picture/networStatus_connect.PNG) หมายถึงการเชื่อมต่อระหว่าง PC และ raspberry pi ปกติ และสามารถสั่งการระบบได้
4.	หากการเชื่อมต่อถูกตัด raspberry pi (pump control) จะสั่งการให้ pump เข้าสู่สถานะ ปิด และ speed 70% โดยจะสั่งการ pump ด้วยคำสั่งนี้อย่างต่อเนื่อง จนกระทั้งสามารถเชื่อมต่อดิอีกครั้ง GUI จะสั่งการให้ pump control เข้าสู่สถานะปิด ใน speed 70% 

*** หากไม่มั่นใจ หรือเกิดปัญหาว่าระบบตอบสนองถูกต้องหรือไม่ให้ทำการปิด-เปิด โปรแกรมใหม่


# Technical manual
System archetecture
![connection_blockDiagram](./picture/connection_blockDiagram.PNG)


### Program flow
![startupSequence_GUI](./picture/startupSequence_GUI.png)

![programFlow_GUI](./picture/programFlow_GUI.png)


## การแก้ไข Program ผ่าน Ethernet (SSH)
1. เปิด SSH tool เช่น Putty
2. IP : 192.168.90.103 (ผู้เขียนตั้ง IP นี้ แต่อาจมีการเปลี่ยนแปลงโดยผู้ควบคุมระบบคนอื่นโดยไม่ได้แก้ไขในเอกสารนี้)
3. User name : pi
4. Password : raspberry
5. คำสั่งเพื่อเข้าถึงไฟล์ที่ต้องการแก้ไข  "sudo nano Desktop/L_Band__VacumnPumpControl.py"
6. การแก้ไข Code ตามต้องการ

