### Send spacial CMD to set initial state after :: stop pump & (unknown standby state)
### tcpip server comunication  (Raspberry pi) 
# <  python L_Band__VacumnPumpControl_sim.py  >
import socket
#import RPi.GPIO as GPIO
import time as timing
import threading
import subprocess

## GPIO control
start_GPIO_pin        = 5
stop_GPIO_pin         = 6
stanbdyStart_GPIO_pin = 19
stanbdyStop_GPIO_pin  = 13
ReadFail_GPIO_pin     = 20
ReadNormal_GPIO_pin   = 21

# ## setup GPIO
# GPIO.setmode(GPIO.BCM)

# # command pump pin

# GPIO.setup(start_GPIO_pin       , GPIO.OUT ,initial = GPIO.LOW)
# GPIO.setup(stop_GPIO_pin        , GPIO.OUT ,initial = GPIO.LOW)
# GPIO.setup(stanbdyStart_GPIO_pin, GPIO.OUT ,initial = GPIO.LOW)
# GPIO.setup(stanbdyStop_GPIO_pin , GPIO.OUT ,initial = GPIO.LOW)

# # read pump pin
# GPIO.setup(ReadFail_GPIO_pin   , GPIO.IN  ,pull_up_down = GPIO.PUD_DOWN)
# GPIO.setup(ReadNormal_GPIO_pin , GPIO.IN  ,pull_up_down = GPIO.PUD_DOWN)

ReadFail_GPIO_pin_state   = 0
ReadNormal_GPIO_pin_state = 0

# set pulse wide of control signal
pulseWide = 1 # sec

# Force Pump initial in stop pump & standby mode state
# GPIO.output(stop_GPIO_pin, GPIO.HIGH)
# timing.sleep(pulseWide)
# GPIO.output(stop_GPIO_pin, GPIO.LOW)
# GPIO.output(stanbdyStart_GPIO_pin, GPIO.HIGH)
# timing.sleep(pulseWide)
# GPIO.output(stanbdyStart_GPIO_pin, GPIO.LOW)

# TCP/IP
TCP_IP      = '127.0.0.1' #'192.168.90.103' #server IP address
TCP_PORT    = 5005
BUFFER_SIZE = 1  # Normally 1024, but we want fast response


# if any command reach server(raspberry pi) this flag will set
ACK_found = True # if can't clear this flag in time pump are going to off & standby mode enable

def Pump_WatchDog():
    ### Note :
    #   create Pulse Wide using time.sleep() in main while loop ONLY
    #       in order to prevent on/off & standby enable/disable at the same time
    #       I can prevent that case using this thread BUT it is going to increase program complexcity(harder to debug).
    
    ### global variable section
    global ACK_found
    
    ### timer setting section
    WatchDog_ack = {
        'MaxTime_limit'       : pulseWide + 1,   # max timer compare  (sec.)   THIS VALUE MUST HAVE VALUE MORE THAN "pulseWide" TO PREVENT SET TO INITIAL STATE(pump off&standby mode enable) 
        'TimeCounter_current' : 0    # timer counter
    }
    
    TimeStep_resolution = 0.5;       # resolution of time (sec.)
    
    while(True):
        ### timer update section
        timing.sleep(TimeStep_resolution)
        
        ### Process section
        # Process "WatchDog_ack"
        if ACK_found == True:
            WatchDog_ack['TimeCounter_current'] = 0
            ACK_found = False
        else:
            if WatchDog_ack['TimeCounter_current'] >= WatchDog_ack['MaxTime_limit']:
                ## set pump off&standby mode enable
                # GPIO.output(stop_GPIO_pin, GPIO.HIGH)
                # timing.sleep(pulseWide)
                # GPIO.output(stop_GPIO_pin, GPIO.LOW)
                # GPIO.output(stanbdyStart_GPIO_pin, GPIO.HIGH)
                # timing.sleep(pulseWide)
                # GPIO.output(stanbdyStart_GPIO_pin, GPIO.LOW)
                print("Shutdown")
            else:
                ## increase count
                WatchDog_ack['TimeCounter_current'] = WatchDog_ack['TimeCounter_current'] + TimeStep_resolution
                
# loop wait for connect LAN wire or other error
#while True:
#    ## Search for raspberry pi static IP address (must connect to Ethernet/network via RJ45 only)
#    output_CMD      = subprocess.check_output("hostname -I", shell=True).decode()
#    IP_address_list = output_CMD.split(" ")
#    TCP_IP          = IP_address_list[0]
#    #print("Raspberry Pi static IP address : ", TCP_IP)
#    
#    ### check network connection
#    try:
#        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#        server.bind((TCP_IP, TCP_PORT))
#        server.listen(3)
#        break
#    except:
#        #print("LAN port disconnect...")
#        timing.sleep(1)


### WatchDog of pump start
Pump_WatchDog_thread = threading.Thread(target=Pump_WatchDog, daemon = True)
Pump_WatchDog_thread.start() 

### Main program
#print("Server start (Raspberry Pi)")

## Test only
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind((TCP_IP, TCP_PORT))
server.listen(3)
while True:
    try:
        # connect to server
        conn, addr = server.accept()
        print('Connection address:', addr)
        data = conn.recv(BUFFER_SIZE)

        # decode cmd for respond
        dataDecode = data.decode()
        ACK_found  = True               # reset watch dog
        #print("received data:", dataDecode) # send ASCII code

        #### action
        # command
        if dataDecode   == '1':   # start pump
            # GPIO.output(start_GPIO_pin, GPIO.HIGH)
            # timing.sleep(pulseWide)
            # GPIO.output(start_GPIO_pin, GPIO.LOW)   
            conn.sendall(str.encode(dataDecode))  
            print("on pump")
        elif dataDecode == '2':   # stop pump
            # GPIO.output(stop_GPIO_pin, GPIO.HIGH)
            # timing.sleep(pulseWide)
            # GPIO.output(stop_GPIO_pin, GPIO.LOW)
            conn.sendall(str.encode(dataDecode))  
            print("off pump")
        elif dataDecode == '3':   # full power mode
            # GPIO.output(stanbdyStop_GPIO_pin, GPIO.HIGH)
            # timing.sleep(pulseWide)
            # GPIO.output(stanbdyStop_GPIO_pin, GPIO.LOW)
            conn.sendall(str.encode(dataDecode))  
            print("standby off")
        elif dataDecode == '4':   # standby mode
            # GPIO.output(stanbdyStart_GPIO_pin, GPIO.HIGH)
            # timing.sleep(pulseWide)
            # GPIO.output(stanbdyStart_GPIO_pin, GPIO.LOW)
            conn.sendall(str.encode(dataDecode)) 
            print("standby on")
        # ask for current pump state
        elif dataDecode == '5': 
            # read "pump normal status wire" & "pump fail status wire"
            #NormalStatusWire = GPIO.input(ReadNormal_GPIO_pin)
            #FailStatusWire   = GPIO.input(ReadFail_GPIO_pin)
            NormalStatusWire = 0
            FailStatusWire   = 1
            
            # set of all posible respond
            wire = (NormalStatusWire << 1) + FailStatusWire
            #print("status : ", wire)
            if wire == 0:    # pump off
                conn.sendall(str.encode('A'))  
            elif wire == 1:  # pump fail
                conn.sendall(str.encode('B')) 
            elif wire == 2:  # pump on
                conn.sendall(str.encode('C')) 
            elif wire == 3:  # short circuit
                conn.sendall(str.encode('D')) 

        # set pump to known state --> stop pump & standby mode
        elif dataDecode == '6': 
            # GPIO.output(stop_GPIO_pin, GPIO.HIGH)
            # timing.sleep(pulseWide)
            # GPIO.output(stop_GPIO_pin, GPIO.LOW)
            # GPIO.output(stanbdyStart_GPIO_pin, GPIO.HIGH)
            # timing.sleep(pulseWide)
            # GPIO.output(stanbdyStart_GPIO_pin, GPIO.LOW)
            conn.sendall(str.encode(dataDecode))
            print("initial state")
        # not recognize command
        else: 
            conn.sendall(str.encode('x'))  # respond

    except:
        print("connection error : client down")
        
        # GPIO.output(stop_GPIO_pin, GPIO.HIGH)
        # timing.sleep(pulseWide)
        # GPIO.output(stop_GPIO_pin, GPIO.LOW)
        # GPIO.output(stanbdyStart_GPIO_pin, GPIO.HIGH)
        # timing.sleep(pulseWide)
        # GPIO.output(stanbdyStart_GPIO_pin, GPIO.LOW)
    timing.sleep(1)

server.close()

# GPIO.output(stop_GPIO_pin, GPIO.HIGH)
# timing.sleep(pulseWide)
# GPIO.output(stop_GPIO_pin, GPIO.LOW)
# GPIO.output(stanbdyStart_GPIO_pin, GPIO.HIGH)
# timing.sleep(pulseWide)
# GPIO.output(stanbdyStart_GPIO_pin, GPIO.LOW)

