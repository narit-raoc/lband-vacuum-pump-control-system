# -*- coding: utf-8 -*-
"""
Created on Tue Oct 26 14:31:35 2021

@author: Nutdanai
"""

from PyQt5 import QtCore, QtWidgets
from PyQt5 import uic
import sys
import time
import socket  

##################### Global data #####################
### Control button status
OnStatus         = False  # tell current status of On-Off button
StandbyStatus    = True   # tell current status of Standby button (on(True) => 70%, off(False) => 100%)
CMD_flag         = True   # tell if user try to control pump
payload          = '4'    # payload
CMD_type_flag    = 0      # tell current type of command ---> 0(on/off), 1(standby mode)
WaitForReply     = False  # 

### pump Status indicator (defult : off        'A')
pumpStatus             = 'A'
### Connection indicator  (defult : disconnect 'C')
connectionStatus       = 'C'
### OnStatus              (defult : off        '2')
OnStatus_readBack      = '2' 
### StandbyStatus         (defult : enable     '4')
StandbyStatus_readBack = '4'

RestartPump = True

### TCP/IP
TCP_IP      = '1270.0.0.1' #'192.168.90.103'  # Connect to this server IP
TCP_PORT    = 5005
BUFFER_SIZE = 1

### Status
PUMP_ON               = '1'
PUMP_OFF              = '2'

STANDBY_OFF       = '3'
STANDBY_ON        = '4'

PUMP_STATUS_CHECK    = '5'

INITIAL_STATUS        = '6'

CONNECTION_CONNECT    = 'A'
CONNECTION_UNSTABLE   = 'B'
CONNECTION_DISCONNECT = 'C'

PUMP_STATUS_OFF       = 'A'
PUMP_STATUS_FAIL      = 'B'
PUMP_STATUS_NORMAL    = 'C'
PUMP_STATUS_SHORT     = 'D'
######################################################

class MainWindows(QtWidgets.QMainWindow): # class for main appication
    def __init__(self):
        global OnStatus, StandbyStatus, payload, CMD_flag
        QtWidgets.QMainWindow.__init__(self)     
        #self.ui = uic.loadUi('GUI_frontPanel.ui',self)         # load GUI name "threads.ui" from same directory
        self.ui = uic.loadUi('GUI_frontPanel.ui',self)
        self.setWindowTitle("L-Band Vacuum pump control")
        self.resize(400, 450)                              # set size to 880(length) * 200(high)  pixel
        
        ## Button&indy name
        # pumpOn_button             # control on push button
        # pumpOff_button            # control off push button
        
        # Speed70_button            # control 70% speed push button
        # Speed100_button           # control 100% speed push button
        
        # pumpOnOff_statusButton    # indicator
        # standbyMode_statusButton  # indicator
        # pumpStatus_indy           # indicator
        # Connection_indy           # indicator
        
        ### bind all control button to function
        self.pumpOn_button.clicked.connect(self.PumpOnOff)
        self.pumpOff_button.clicked.connect(self.PumpOnOff)
        self.Speed70_button.clicked.connect(self.standbyModeOnOff)
        self.Speed100_button.clicked.connect(self.standbyModeOnOff)
        
        
        ### set strat button appearance (match with initial state)
        self.pumpOn_button.setText("On") 
        self.pumpOff_button.setText("Off") 
        self.pumpOnOff_statusButton.setStyleSheet('border : 3px solid #ffffff;'
                                                  'background-color : #808080')
        self.Speed70_button.setStyleSheet('border : 1px solid #ffffff;' 
                                          'background-color : #B2FF66');   self.Speed70_button.setText("70%")
        self.Speed100_button.setText("100%")
        self.pumpStatus_indy.setStyleSheet("background-color : #FF0000"); self.pumpStatus_indy.setText("Fail")
        self.Connection_indy.setStyleSheet("background-color : #FF0000"); self.Connection_indy.setText("Disconnect")
          
        ## enable all button& indicator
        self.pumpOn_button.setEnabled(True)
        self.pumpOff_button.setEnabled(False)
        self.Speed70_button.setEnabled(True)
        self.Speed100_button.setEnabled(False)
        self.pumpStatus_indy.setEnabled(True)
        self.Connection_indy.setEnabled(True) 
        
        ## update button appearence
        # update on/off button 
        OnStatus         = False
        # update standbyMode button
        StandbyStatus    = True
        
        
        ## after gain connection "ALWAYS" stop pump & standby mode
        payload   = INITIAL_STATUS
        CMD_flag  = True
        
        ## enable thread
        self.worker = ThreadClass(parent=None,index=1)        # passing index to the worker class("ThreadClass" class)
        self.worker.start()                                   # start thread (by call "start()" function in "ThreadClass")
        self.worker.any_signal.connect(self.Update_indicator) # bind thread with "Update_indicator" function
        
        
    def PumpOnOff(self):
        ## CMD 
        global OnStatus, CMD_flag, payload, CMD_type_flag
        global PUMP_ON , PUMP_OFF
        
        global RestartPump
        global WaitForReply
        
        if WaitForReply == False:
            OnStatus = not(OnStatus)
        CMD_flag = True
        CMD_type_flag = 0
        
        ## update button appearence
        if OnStatus == True:
            payload = PUMP_ON
        else:
            payload = PUMP_OFF
            RestartPump = True
        
        ## update On/Off button appearence
        if OnStatus == False:
            self.pumpOn_button.setText("On")
            self.pumpOff_button.setText("Off")
            self.pumpOn_button.setEnabled(True)
            self.pumpOff_button.setEnabled(False)
        elif OnStatus == True:
            self.pumpOn_button.setText("On")
            self.pumpOff_button.setText("Off")
            self.pumpOn_button.setEnabled(False)
            self.pumpOff_button.setEnabled(True)
        else:
            self.pumpOn_button.setText("Error")
            self.pumpOff_button.setText("Error")
            
            
    def standbyModeOnOff(self):
        ### CMD
        global StandbyStatus  , CMD_flag , payload, CMD_type_flag
        global STANDBY_OFF    , STANDBY_ON
        
        global WaitForReply
        
        if WaitForReply == False:
            StandbyStatus = not(StandbyStatus)
        CMD_flag = True
        CMD_type_flag = 1 
        
        ## update button command
        if StandbyStatus == True:
            payload = STANDBY_ON
        else:
            payload = STANDBY_OFF
        
        ## update Standby button appearence
        if StandbyStatus == False:
            self.Speed70_button.setText("70%")
            self.Speed100_button.setText("100%")  
            self.Speed70_button.setEnabled(False)
            self.Speed100_button.setEnabled(True)
        elif StandbyStatus == True: 
            self.Speed70_button.setText("70%")
            self.Speed100_button.setText("100%")  
            self.Speed70_button.setEnabled(True)
            self.Speed100_button.setEnabled(False)
        else:
            self.Speed70_button.setText("Error")
            self.Speed100_button.setText("Error")
            
            
    ## update indication (call by thread)
    def Update_indicator(self, count):
        global PUMP_ON           , PUMP_OFF
        global STANDBY_OFF       , STANDBY_ON
        global PUMP_STATUS_CHECK, INITIAL_STATUS
        global PUMP_STATUS_OFF   , PUMP_STATUS_FAIL   , PUMP_STATUS_NORMAL   , PUMP_STATUS_SHORT
        global CONNECTION_CONNECT, CONNECTION_UNSTABLE, CONNECTION_DISCONNECT

        global pumpStatus        , connectionStatus
        global OnStatus_readBack , StandbyStatus_readBack
        global OnStatus          , StandbyStatus
        
        ## On/Off control button update  
        ## update On/Off button appearence
        if OnStatus == False:
            self.pumpOn_button.setText("On")
            self.pumpOff_button.setText("Off")
            self.pumpOn_button.setEnabled(True)
            self.pumpOff_button.setEnabled(False)
        elif OnStatus == True:
            self.pumpOn_button.setText("On")
            self.pumpOff_button.setText("Off")

        else:
            self.pumpOn_button.setStyleSheet("background-color : #FFFFFF");  self.pumpOn_button.setText("Error")
            self.pumpOff_button.setStyleSheet("background-color : #FFFFFF"); self.pumpOff_button.setText("Error")
           
        ## On/Off status indicator
        if OnStatus_readBack == PUMP_ON:
            self.pumpOnOff_statusButton.setStyleSheet('border : 3px solid #ffffff;'
                                                      'background-color: '
                                                      'qlineargradient('
                                                      'spread:'
                                                      'pad, x1:0, y1:0.5, x2:0, y2:1,'
                                                      'stop:0 #00cc00,'
                                                      'stop:1 #a0a0a0);')
            self.pumpOnOff_statusButton.setText("On")
        elif OnStatus_readBack == PUMP_OFF:
            self.pumpOnOff_statusButton.setStyleSheet('border : 3px solid #ffffff;'
                                                      'background-color : #808080')
            self.pumpOnOff_statusButton.setText("Off")
        else:
            self.pumpOnOff_statusButton.setStyleSheet("background-color : #FFFFFF"); 
            self.pumpOnOff_statusButton.setText("Error")
        
        ## Standby mode On/Off status indicator
        if OnStatus == False:
            self.Speed70_button.setStyleSheet('border : 1px solid #ffffff;' 
                                              'background-color : #B2FF66');   self.Speed70_button.setText("70%");
            self.Speed100_button.setStyleSheet('border : 1px solid #ffffff;'
                                               'background-color : #E0E0E0');  self.Speed100_button.setText("100%");      
            self.Speed70_button.setEnabled(False)
            self.Speed100_button.setEnabled(False)
        else:
            if StandbyStatus_readBack == STANDBY_OFF:
                self.Speed70_button.setStyleSheet('border : 1px solid #ffffff;' 
                                                  "background-color : #C0C0C0"); 
                self.Speed100_button.setStyleSheet('border : 1px solid #ffffff;' 
                                                   "background-color : #00CC00"); 
                self.Speed70_button.setEnabled(True)
                self.Speed100_button.setEnabled(False)
            elif StandbyStatus_readBack == STANDBY_ON:
                self.Speed70_button.setStyleSheet('border : 1px solid #ffffff;'   
                                                  "background-color : #00CC00");  
                self.Speed100_button.setStyleSheet('border : 1px solid #ffffff;'  
                                                   "background-color : #C0C0C0"); 
                self.Speed70_button.setEnabled(False)
                self.Speed100_button.setEnabled(True)
            else:
                self.Speed70_button.setStyleSheet('border : 1px solid #ffffff;' 
                                                  "background-color : #FFFFFF"); 
                self.Speed100_button.setStyleSheet('border : 1px solid #ffffff;' 
                                                   "background-color : #FFFFFF");
        
        ## pump status update
        if pumpStatus == PUMP_STATUS_OFF:
            self.pumpStatus_indy.setStyleSheet("background-color : #FF8900"); self.pumpStatus_indy.setText("Full Speed") 
        elif pumpStatus == PUMP_STATUS_FAIL:
            self.pumpStatus_indy.setStyleSheet("background-color : #FF0000"); self.pumpStatus_indy.setText("Fail") 
        elif pumpStatus == PUMP_STATUS_NORMAL:
            self.pumpStatus_indy.setStyleSheet("background-color : #00FF00"); self.pumpStatus_indy.setText("Normal") 
        elif pumpStatus == PUMP_STATUS_SHORT:
            self.pumpStatus_indy.setStyleSheet("background-color : #00FFF7"); self.pumpStatus_indy.setText("Short") 
        else:
            self.pumpStatus_indy.setStyleSheet("background-color : #FFFFFF"); self.pumpStatus_indy.setText("Error") 
            
        ## connection status update
        if connectionStatus == CONNECTION_CONNECT:
            self.Connection_indy.setStyleSheet("background-color : #00FF00"); self.Connection_indy.setText("Connected")
        elif connectionStatus == CONNECTION_UNSTABLE:
            self.Connection_indy.setStyleSheet("background-color : #F7FF00"); self.Connection_indy.setText("Unstable")
        elif connectionStatus == CONNECTION_DISCONNECT:
            self.Connection_indy.setStyleSheet("background-color : #FF0000"); self.Connection_indy.setText("Disconnect")
        else:
            self.Connection_indy.setStyleSheet("background-color : #FFFFFF"); self.Connection_indy.setText("Error")
        
        
### Thread operation class
class ThreadClass(QtCore.QThread):            # thread sub class
    any_signal = QtCore.pyqtSignal(int)       # create signal call "any_signal" 
    
    def __init__(self, parent=None,index=0):
        super(ThreadClass, self).__init__(parent)
        self.index=index            # set index of each thread as input
        self.is_running = True      # set thread to run
        
    def run(self):
        # global valuable
        global payload           , CMD_flag              , CMD_type_flag   # send payload
        global pumpStatus        , connectionStatus                        # return data to update indicator
        global OnStatus_readBack , StandbyStatus_readBack                  # return data to update indicator
        global StandbyStatus     , OnStatus                                # for reconfig control button
        global TCP_IP            , TCP_PORT              , BUFFER_SIZE     # TCPIP parameter
        
        global PUMP_ON           , PUMP_OFF        
        global STANDBY_OFF       , STANDBY_ON
        global PUMP_STATUS_CHECK, INITIAL_STATUS
        global PUMP_STATUS_OFF   , PUMP_STATUS_FAIL   , PUMP_STATUS_NORMAL   , PUMP_STATUS_SHORT
        global CONNECTION_CONNECT, CONNECTION_UNSTABLE, CONNECTION_DISCONNECT
        
        global RestartPump
        
        global WaitForReply
        
        ## local valuable
        # discconect checking
        disconnectCount       = 0    # count unsuccessful/unstable connection
        disconnectCount_limit = 3    # if can't onnected more than this value THAT MEAN "Disconnect"
        disconnectFlag        = True # set to "true" when unale to connect 
     
        # count time
        count_limit  = 10            # time limit (0.1 sec.)
        count        = count_limit   # after gain connection "ALWAYS" stop pump & standby mode
        samplingTime = 0.1           # time resolusion (sec.)
        
        # process
        while True:
            if CMD_flag == True:  # evet process
                #WaitForReply = True
                ############# TCP/IP ##############
                try:
                    # try to connect to server(Raspberry pi)
                    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    client.connect((TCP_IP, TCP_PORT))
    
                    # send data to server
                    if disconnectFlag == True or RestartPump == True:  # after gain connection "ALWAYS" stop pump & standby mode
                        payload     = INITIAL_STATUS
                        
                        RestartPump            = False
                        OnStatus_readBack      = PUMP_OFF
                        StandbyStatus_readBack = STANDBY_ON
                        StandbyStatus          = True
                        OnStatus               = False
                    client.send(str.encode(payload))
    
                    # recieve respond data
                    WaitForReply = True
                    data = client.recv(BUFFER_SIZE)
                    dataDecode = data.decode()
                    WaitForReply = False
    
                    # checked respond
                    connectionStatus  = CONNECTION_CONNECT  # connected
                    if payload != dataDecode:
                        if (disconnectFlag == True) & (payload == dataDecode):
                            # display by defult
                            OnStatus_readBack      = PUMP_OFF
                            StandbyStatus_readBack = STANDBY_ON
                            StandbyStatus          = True
                            OnStatus               = False
                        else:
                            disconnectFlag   = True
                            connectionStatus = CONNECTION_UNSTABLE   # communication unstable
                    else:
                        if dataDecode == INITIAL_STATUS:
                            OnStatus_readBack      = PUMP_OFF
                            StandbyStatus_readBack = STANDBY_ON
                        elif CMD_type_flag == 0:
                            OnStatus_readBack = dataDecode
                        elif CMD_type_flag == 1:
                            StandbyStatus_readBack = dataDecode
                        else: # command fail
                            OnStatus_readBack      = 'x' 
                            StandbyStatus_readBack = 'x' 
    
                    # check disconnect flag
                    disconnectCount = 0
                    disconnectFlag  = False
    
                    # close connection
                    client.close()
                except:
                    if disconnectCount >= disconnectCount_limit:
                        disconnectCount = disconnectCount + 1
                        disconnectFlag  = True
                        connectionStatus = CONNECTION_UNSTABLE   # communication unstable
                    else:
                        disconnectFlag  = True
                        disconnectCount = disconnectCount + 1
                        connectionStatus = CONNECTION_DISCONNECT   # communication disconnect
                ####################################
    
                CMD_flag = False
            else:
                if count >= count_limit: # ruteen process
                    #WaitForReply = True
                ############# TCP/IP ##############
                    try:
                        # try to connect to server(Raspberry pi)
                        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        client.connect((TCP_IP, TCP_PORT))
    
                        # send data to server
                        payload = PUMP_STATUS_CHECK 
    
                        if disconnectFlag == True:  # after gain connection "ALWAYS" stop pump & standby mode
                            payload = INITIAL_STATUS
                        client.send(str.encode(payload))
    
                        # recieve respond data
                        WaitForReply = True
                        data = client.recv(BUFFER_SIZE)
                        dataDecode = data.decode()
                        WaitForReply = False
    
                        # checked respond
                        connectionStatus  = CONNECTION_CONNECT  # connected
                        if dataDecode   == PUMP_STATUS_OFF:     # pump off
                            pumpStatus = dataDecode
                        elif dataDecode == PUMP_STATUS_FAIL:    # pump fail
                            pumpStatus = dataDecode
                        elif dataDecode == PUMP_STATUS_NORMAL:  # pump normal
                            pumpStatus = dataDecode
                        elif dataDecode == PUMP_STATUS_SHORT:   #short circuit
                            pumpStatus = dataDecode
                        elif dataDecode == 'x':                 # don't undrestand command
                            connectionStatus = CONNECTION_UNSTABLE   # communication unstable
                        else:
                            if (disconnectFlag == True) & (payload == dataDecode):
                                OnStatus_readBack      = PUMP_OFF
                                StandbyStatus_readBack = STANDBY_ON
                                StandbyStatus          = True
                                OnStatus               = False
                            else:
                                connectionStatus = CONNECTION_UNSTABLE   # communication unstable
    
                        # check disconnect flag
                        disconnectCount = 0
                        disconnectFlag  = False
    
                        # close connection
                        client.close()
                    except:
                        if disconnectCount >= disconnectCount_limit:
                            disconnectCount = disconnectCount + 1
                            disconnectFlag  = True
                            connectionStatus = CONNECTION_UNSTABLE   # communication unstable
                        else:
                            disconnectFlag  = True
                            disconnectCount = disconnectCount + 1
                            connectionStatus = CONNECTION_DISCONNECT   # communication disconnect
                ####################################
                
                    count = 0
                else:
                    count = count + 1
            
            # sampling time
            time.sleep(samplingTime)  # sec.
            
            # send data to activate task
            self.any_signal.emit(count)  # send data from this thread for update all element of GUI
            
    def stop(self): 
        self.is_running = False        # set thread to stop
        self.terminate()               # terminat the runnig thread
            
####### Run program #######
app = QtWidgets.QApplication(sys.argv)
mainWindow = MainWindows()      
mainWindow.show()                          # show GUI
app.exec_()                                # Start Qt Event loop (including open GUI window)
ThreadClass().stop()                       # stop thread